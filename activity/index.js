
/*Activity Instruction:
	1. In the S17 folder, create an activity folder, an index.html file inside of it and link the index.js file.
	2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
	3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
	4.  Create a function which is able to prompt the user to provide their full name, age, and location. 
		- use prompt() and store the returned value into function scoped variables within the function. 
		- show an alert to thank the user for their input.
		- display the user's inputs in messages in the console.
 		- invoke the function to display the user’s information in the console.
		- follow the naming conventions for functions.
	5. Create a function which is able to print/display your top 5 favorite bands/musical artists. 
		- invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	6. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating. 
		- look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
        - invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	7. Debugging Practice - Debug the given codes and functions to avoid errors.
		- check the variable names.
		- check the variable scope.
		- check function invocation/declaration.
		- comment out unusable codes.
	8. Create a git repository named S17.
 	9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	10. Add the link in Boodle.

	How you will be evaluated
	1. No errors should be logged in the console.
	2. prompt() is used to gather information.
	3. alert() is used to show information.
	4. All values must be properly logged in the console.
	5. All variables are named appropriately and defines the value it contains.
	6. All functions are named appropriately and follows naming conventions.

 */

// console.log("Hello World");

function userInformation(){
	let fullName = prompt('What is your name? ');
	let age = prompt('How old are you? ');
	let location = prompt('Where do you live? ');

	console.log("Hello, " + fullName + ".");
	console.log("You are " + age + " years old.");
	console.log("You live in " + location + ".");
};

userInformation();

function thankYouMessage(){
	alert("Thank you!");
};

thankYouMessage();

function myFavMetalBands(){
	let band1 = ("Cradle of Filth");
	let band2 = ("Deicide");
	let band3 = ("Slayer");
	let band4 = ("Slipknot");
	let band5 = ("Behemoth");



	console.log("My favorite bands: ");
	console.log("1. " + band1);
	console.log("2. " + band1);
	console.log("3. " + band1);
	console.log("4. " + band1);
	console.log("5. " + band1);
};

myFavMetalBands();

function myTopFiveMoviesaAndRatings(){
	let Movie1 = ("The NoteBook");
	let Movie2 = ("Hitch");
	let Movie3 = ("Love Actually");
	let Movie4 = ("About a boy");
	let Movie5 = ("Spider-Man");

	let rating1 = ("53%");
	let rating2 = ("69%");
	let rating3 = ("64%");
	let rating4 = ("93%");
	let rating5 = ("90%");

	console.log("My Top 5 favorite movies: ");
	console.log("1. " + Movie1);
	console.log("Rotten Tomatoes Rating: " + rating1);
	console.log("2. " + Movie2);
	console.log("Rotten Tomatoes Rating: " + rating2);
	console.log("3. " + Movie3);
	console.log("Rotten Tomatoes Rating: " + rating3);
	console.log("4. " + Movie4);
	console.log("Rotten Tomatoes Rating: " + rating4);
	console.log("5. " + Movie5);
	console.log("Rotten Tomatoes Rating: " + rating5);
};

myTopFiveMoviesaAndRatings();









let printFriends = function printUsers(){
	let displayPopUp = alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

